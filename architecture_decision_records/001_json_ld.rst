ADR-001: JSON-LD as the Exchange Format for Self-Descriptions
=============================================================

:adr-id: 001
:revnumber: 1.0 
:revdate: 06-07-2020
:status: accepted
:author: Self-Description WP, Catalogue WP
:stakeholder: Self-Description WP, Catalogue WP

Summary
-------

GAIA-X needs to define a serialization format for the exchange of
Self-Descriptions. The exchange format allows the Self-Descriptions to be
serialized into files for transportation between Services and Catalogues.
JSON-LD is selected as the Self-Description serialization format.

The following is a minimal example for the JSON-LD format::

    {
        "@context": "https://json-ld.org/contexts/gaia-x.jsonld",
        "@id": "http://dbpedia.org/resource/MyService",
        "provider": "http://dbpedia.org/resource/MyProvider",
        "name": "MyService"
    }

Linked Data Proofs 1.0 (https://w3c-ccg.github.io/ld-proofs/, currently in draft
status).

An example for a signed document with Linked Data Proofs is this::

    {
      "@context": "https://www.w3.org/2018/credentials/examples/v1",
      "title": "Hello World!",
      "proof": {
        "type": "Ed25519Signature2018",
        "proofPurpose": "assertionMethod",
        "created": "2019-08-23T20:21:34Z",
        "verificationMethod": "did:example:123456#key1",
        "domain": "example.org",
        "jws": "eyJ0eXAiOiJK...gFWFOEjXk"
      }
    }

Context
-------

The Architecture Document (June 2020) states that Self-Descriptions are
expressed in an extensible format.

JSON is an established data serialization format. It is both human-readable and
machine-interpretable.

JSON-LD combines JSON with semantic technologies (ontologies) from the Linked
Data community. Specifically, the RDF-Standard is referenced. So the
schema-definitions (and tooling) from the established RDF format can be reused.

Alternative Technologies
~~~~~~~~~~~~~~~~~~~~~~~~

Decision Statements
-------------------

The serialization format for the exchange of GAIA-X Self-Descriptions is JSON-LD
1.1.

The RDF 1.1-standard is used to express an extensible hierarchy of schemas for
Self-Descriptions with well-known attributes.

Cryptographic signatures are added to Self-Descriptions according to the Linked
Data Proofs 1.0 specification.

Consequences
------------

All Self-Descriptions need to be ready for serialization into the JSON-LD
format.

The extensible hierarchy of Self-Description attributes is expressed in an
ontology according to the RDF standard.

The Identifier of GAIA-X Assets and Participants must be IRIs (Internationalized
Resource Identifiers [RFC3987]) so that they can be used for cross-referencing
between Self-Descriptions in the JSON-LD format.

ADR References
--------------

External References
-------------------

* [JSON-LD] JSON-LD 1.1 - A JSON-based Serialization for Linked Data,
  https://www.w3.org/TR/json-ld11
* [LDP] Linked Data Proofs 1.0, https://w3c-ccg.github.io/ld-proofs/
* [RDF] RDF 1.1 Concepts and Abstract Syntax,
  https://www.w3.org/TR/rdf11-concepts/
