#!/usr/bin/env python3

# import requests_cache
from collections import defaultdict
import re

from datetime import timedelta
import tempfile
import shutil
from datetime import timedelta

# requests_cache.install_cache('cache', expire_after=timedelta(hours=1))

import gitlab
import os

from colorama import init, Fore, Back, Style
init()

def _list(obj, **kwargs):
    result = [] 
    page = 1
    while len(obj.list(page=page, per_page=100, **kwargs)):
        result += obj.list(page=page, per_page=100, **kwargs)
        page += 1
    return result

if __name__ == '__main__':
    GITLAB_HOST = os.environ['GITLAB_HOST']
    GITLAB_TOKEN = os.environ['GITLAB_TOKEN']
    CI_PROJECT_ID = os.environ['CI_PROJECT_ID']

    gl = gitlab.Gitlab(GITLAB_HOST, private_token=GITLAB_TOKEN)
    mr_open = defaultdict(int)

    re_hunk = re.compile(r"^[^@]*@@ -(?P<old_start>[0-9]+)(,(?P<old_len>[0-9]+))? \+(?P<new_start>[0-9]+)(,(?P<new_len>[0-9]+))? @@.*$")

    project = gl.projects.get(CI_PROJECT_ID)
    print(project.path_with_namespace)
    for mr in _list(project.mergerequests, state="opened", wip="no"):
        print(Fore.RED + str(mr.iid) + Style.RESET_ALL)
        latest_diff = mr.diffs.list()[0]
        latest_changes = mr.diffs.get(latest_diff.id)
        for latest_change in latest_changes.diffs:
            if not latest_change['old_path'].endswith('.md'): # not a markdown file
                continue
            print(latest_change['old_path'])
            first_line = latest_change['diff'].split('\n')[0]
            result = re_hunk.match(first_line)
            if result is None: # no match. Could be a binary file
                continue
            if not os.path.exists(latest_change['old_path']):
                continue
            old_start = int(result.groupdict()['old_start'])
            old_len = int(result.groupdict().get('old_len', '1'))
            with tempfile.NamedTemporaryFile(mode="wt", delete=False) as fdout:
                with open(latest_change['old_path'], 'r') as fdin:
                    for count, line in enumerate(fdin):
                        if count == old_start:
                            fdout.write(f'<span class="merge-request"><a class="merge-request" data-iid="{mr.iid}" href="{mr.web_url}"><i class="fas fa-code-branch"></i></a></span>\n' + line)
                        else:
                            fdout.write(line)
            shutil.copy(fdout.name, latest_change['old_path'])
            os.remove(fdout.name)
