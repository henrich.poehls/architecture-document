# Federation Services

The *Federation Services* are necessary to enable a Federation of
infrastructure and data and to provide interoperability across Federations.

-   The *[inter-catalogue synchronisation](#federated-catalogue)* constitutes an indexed
    repository of Gaia-X Self-Descriptions to enable the discovery and
    selection of Providers and their Service Offerings.

-   Common vocabulary for *[Identity and Access Management](#identity-and-access-management)* covers identification, authentication and
    authorization, credential management, decentralized Identity management
    as well as the verification of analogue credentials, including between existing identity federation.

-   *[Data Exchange services](#data-exchange-services)* enable data
    exchange of Participants by providing a Data Agreement Service and a
    Data Logging Service to enable the enforcement of Policies. Furthermore,
    usage constraints for data exchange can be expressed by Provider
    Policies within the Self-Descriptions.

-   *[Gaia-X Trust Framework](#gaia-x-trust-framework)* includes mechanisms to ensure that
    Participants adhere to the Policy Rules in areas such as
    security, privacy, transparency and interoperability during
    onboarding and service delivery.

-   *[Portals and APIs](#portals-and-apis)* will support onboarding and
    management of Participants, demonstrate service discovery,
    orchestration and provisioning of sample services within ecosystems.

## The Role of Federation Services for ecosystems

The following figure visualizes how Federation Services Instances
are related to the Federator described in the conceptual model (see
section [Federator](conceptual_model.md#federator)). The Federators enable Federation Services by
obliging Federation Service Providers to provide concrete
Federation Service Instances. The sum of all Federation Service
Instances form the Federation Services.

![](figures/image12.png)
*Federation Services Relations*

### Nesting and Cascading of Federation Services

Federation Services can be nested and cascaded. Cascading is needed, for example, to ensure uniqueness of identities and Catalogue entries across different individual ecosystems / communities that use Federation Services. (Comparable to DNS servers: there are local servers, but information can be pushed up to the root servers).

Therefore, a decentralised synchronization mechanism is necessary.

### Ecosystem Governance vs. Management Operations

To enable interoperability, portability and Data Sovereignty across
different ecosystems and communities, Federation Services need to adhere
to common standards. These standards (e.g., related to service
Self-Description, digital identities, logging of data sharing
transactions, etc.) must be unambiguous and are therefore defined by the
Gaia-X Association. The Gaia-X Association owns the Trust Framework and related
regulations or governance aspects. Different entities may take on the
role of Federator and Federation Services Provider.

### Infrastructure ecosystem

The infrastructure ecosystems have a focus on computing, storage and Interconnection elements. In GAIA-X Ecosystem these elements are designated as Nodes, Interconnections and different Software Resources. They range from low-level services like bare metal computing 
up to highly sophisticated offerings, such as high-performance
computing. Interconnection Services ensure secure and performant data
exchange between the different Providers, Consumers and their services.
Gaia-X enables combinations of services that range across multiple
Providers of the ecosystem. The Interconnection Services are also the key enabler for the composition of services offered by diverse and distributed providers, ensuring the performance of single-provider networks on a multi-provider "composed" network.

### Data ecosystem

Gaia-X facilitates Data Spaces which present a virtual data integration
concept, where data are made available in a decentralised manner, for
example, to combine and share data stored in different cloud storage backends.
In general, data ecosystems enable Participants to leverage data as a strategic resource
in an inter-organizational network without restrictions of a fixed
defined partner or central keystone companies. For data to realize its
full potential, it must be made available in cross-company,
cross-industry ecosystems. Therefore, Data ecosystems not only enable
significant data value chain improvements, but provide the technical means to enable Data
Sovereignty. Such sovereign data sharing addresses different layers and
enables a broad range of business models that would otherwise be impossible. 
Trust and control mechanisms encourage the acceleration of data sharing and proliferate the growth of ecosystems.

### Federation, Distribution, Decentralization and Sharing

The principles of federation, distribution, decentralization and sharing
are emphasized in the Federation Services as they provide several
benefits for the ecosystem:


| Principle        | Need for Gaia-X          | Implemented in Gaia-X Architecture        |
|------------------|--------------------------|-------------------------------------------|
| Decentralization | Decentralization will ensure Gaia-X is not controlled by the few and strengthens the participation of the many. It also adds key technological properties like redundancy, and therefore resilience against unavailability and exploitability. Different implementations of this architecture create a diverse ecosystem that can reflect the respective requirements and strengths of its Participants.<br><br>(example: IP address assignment) | The role of Federators may be taken by diverse actors.<br><br>The open source Federation Services can be used and changed according to specific new requirements as long as they are compliant and tested. |
| Distribution     | Distribution fosters the usage of different Resources by different Providers spread over geographical locations.<br><br>(Example: Domain Name System) | Self-Description ensures that all Resources and Service Offerings are defined in standardized ways, which enables them to be listed in a searchable Catalogue, each with a unique Identifier. Therefore, it facilitates the reuse and distribution of these components. |
| Federation       | Federation technically enables connections and a web of trust between and among different parties in the ecosystem(s). It addresses the following challenges:<ul><li>Decentralized processing locations</li><li>Multiple actors and stakeholders</li><li>Multiple technology stacks</li><li>Special policy requirements or regulated markets</li></ul><br>(Example: Autonomous Systems) | Each system can interact with each other, e.g., the Catalogues could exchange information and the Identity remains unique. Furthermore, different Conformity Assessment Bodies may exist. |
| Sharing          | Sharing of the relevant services and components contributes to the Ecosystem development.<br><br>Sharing and reuse of Resources across the Gaia-X Ecosystem enables positive spillovers, leading to new and often unforeseen economic growth opportunities. | The Federated Catalogues enable the matching between Providers and Consumers. Sovereign Data Exchange lowers hurdles for data exchange and ecosystem creation. |

*Summary of Federation Services as enabler*


By utilizing common specifications and standards, harmonized rules and
policies, Gaia-X is well aligned with specifications like NIST Cloud
Federation Reference Architecture[^25]:

-   Security and collaboration context are not owned by a single entity

-   Participants in the Gaia-X Association jointly agree upon the common goals
    and governance of the Gaia-X Association 

-   Participants can selectively make some of their Resources
    discoverable and accessible by other Participants in compliance with
    Gaia-X

-   Providers can restrict their discovery and disclose certain
    information but could risk losing their Gaia-X compliance level

[^25]: Bohn, R. B., Lee, C. A., & Michel, M. (2020). The NIST Cloud Federation Reference Architecture: Special Publication (NIST SP) - 500-332. NIST Pubs. <https://doi.org/10.6028/NIST.SP.500-332>

## Interoperability and Portability for Infrastructure and Data

For the success of a Federated ecosystem it is of importance that data,
services and the underlying infrastructure can interact seamlessly with each
other. Therefore, portability and interoperability are two key
requirements for the success of Gaia-X as they are the cornerstones for
a working platform and ensure a fully functional federated, multi-provider
environment.

Interoperability is defined as the ability of several systems or
services to exchange information and to use the exchanged information
mutually. Portability refers to the enablement of data transfer and
processing to increase the usefulness of data as a strategic resource.
For services, portability implies that they can be migrated from one
provider to another, while the migration should be possible without
significant changes and adaptations and have an equivalent QoS (Quality of Service). 

### Areas of Interoperability and Portability

The Gaia-X Ecosystem includes a huge variety of Participants and Service Offerings. Therefore, interoperability needs to be ensured on different levels (Infrastructure as a Service [IaaS] , Platform as a Service [PaaS], Software as a Service [Saas], data resources, and others) by means of Service Composition.

Regarding interoperability of data, core elements to be identified in this endeavour are API specifications and best practices for semantic data descriptions. The use of semantic data interoperability is seen as a foundation to eventually create a clear mapping between domain-specific approaches based on a community process and open source efforts.

## Infrastructure and Interconnection

To best accommodate the wide variety of Service Offerings, the Gaia-X
Architecture is based on the notion of a sovereign and flexible
Interconnection of infrastructure and data ecosystems, where data is
flexibly exchanged between and among many different Participants. Therefore,
Interconnection Services represent a dedicated category of Resources as described in
section [Gaia-X Conceptual Model](conceptual_model.md).

The [Interconnection Whitepaper](https://gaia-x.eu/sites/default/files/2022-04/The%20role%20and%20the%20importance%20of%20Interconnection%20in%20Gaia-X_13042022.pdf) provides an overview on the current strategy to expand the Gaia-X architecture and federation services.

## Federated Catalogue

The goal of Catalogues is to
enable Consumers to find best-matching offerings and to monitor for relevant
changes of the offerings. The Providers decide in a self-sovereign manner which
information they want to make public in a Catalogue and which information they
only want to share privately.

A Provider registers Self-Descriptions with their universally resolvable Identifiers in the desired Catalogue to make them public relative to the Catalogue scope.
The Catalogue builds an internal representation of a knowledge graph out of the linked data from the registered and accessible seld-descriptions to provide interfaces to query, search and filter services offerings.

The system of Federated Catalogues includes an initial stateless Self-Description
browser provided by the Gaia-X Association. 
In addition, ecosystem-specific Catalogues (e.g., for the healthcare
domain) and even company-internal Catalogues (with private
Self-Descriptions to be used only internally) can be linked to the system of federated Catalogues. The Catalogue federation is used to exchange relevant Self-Descriptions and updates thereof. It is not used to execute queries in a distributed fashion.

Cross-referencing is enabled by unique Identifiers as described in [Identity and Access Management](federation_service.md#identity-and-access-management).
While uniqueness means that Identifiers do not refer to more than one entity, 
there can be several Identifiers referring to the same entity. 
A Catalogue should not use multiple Identifiers for the same entity.

Gaia-X develops an extensible hierarchy of Schemas that define the terms used in Self-Descriptions
and which must be supported by any Catalogue.
It is possible to create additional Schemas specific to an application domain, an ecosystem, Participants in it, or Resources offered by these Participants.

A Schema may define terms (classes, their attributes, and their relationships to other classes) in an ontology.
If it does, it must also define shapes to validate instances of the ontology against.

Self-Descriptions in a
Catalogue are either loaded directly into a Catalogue or exchanged from
another Catalogue through inter-Catalogue synchronization functions.

Since Self-Descriptions are protected by cryptographic signatures, they
are immutable and cannot be changed once published. This implies that after any changes to a Self-Description, the Participant as the Self-Description issuer has to sign the Self-Description again and release it as a new version.
The lifecycle state of a Self-Description is described in the Self-Description Definition chapter.

## Identity and Access Management

Identities, which are used to gain access to the Ecosystem, rely on
unique Identifiers and a list of dependent attributes. Gaia-X uses existing Identities and does not maintain them directly. Uniqueness is ensured by
a specific Identifier format relying on properties of existing
protocols. The Identifiers are comparable in the raw form and should not 
contain more information than necessary (including Personal Identifiable Information). 
Trust - confidence in the Identity and capabilities of
Participants or Resources - is established by cryptographically
verifying Identities using the Gaia-X Compliance Service, which
guarantees proof of identity of the involved
Participants to make sure that Gaia-X Participants are who they claim to
be. In the context of Identity and Access Management, the digital representation of a natural person, acting on behalf of a Participant, is
referred to as a Principal. As Participants need to trust other
Participants and Service Offerings provided, it is important that the
Gaia-X Trust Framework provides transparency for everyone.
Therefore, proper lifecycle management is required, covering Identity
onboarding, maintaining, and offboarding within Ecosystem. The table below shows the Participant
Lifecycle Process.

| Lifecycle Activity | Description |
|--------------------|-------------|
| Onboarding         | The Gaia-X Compliance Service and optional individual ecosystems workflow validates and signs the Self-Description provided by a Visitor (the future Participant/Principal). |
| Maintaining        | Trust related changes to the Self-Descriptions are recorded in a new version and validated and signed by the same entities involved during the Onboarding. This includes information controlled by the Participant/Principal. |
| Offboarding        | The offboarding process of a Participant is time-constrained and involves all dependent Participants/Principals. This includes keypair revocation by the entities involved during the Onboarding. |

*Participant Lifecycle Process*

An Identity is composed of a unique Identifier and an attribute or set
of attributes that uniquely describe an entity within a given context. The lifetime of an Identifier is permanent. It may be used as a reference to an entity well beyond the lifetime of the entity it identifies or of any naming authority involved in the assignment of its name. Reuse of an Identifier for a different entity is forbidden. 
Attributes will be derived from existing identities as shown in the IAM Framework Document v1.2[^17].

[^17]: See the IAM Framwork version 1.2 for details: https://community.gaia-x.eu/s/P23ZJNLyjf7n7Zp?path=%2FReleases.

A 'Secure Digital Identity' is a unique Identity with additional data for robustly trustworthy authentication of the entity (i.e. with appropriate measures to prevent impersonation). This implies that Gaia-X Participants can self-issue Identifiers for such Identities. It is solely the responsibility of a Participant to determine the conditions under which the Identifier will be issued.  Identifiers shall be derived from the native identifiers of an Identity System without any separate attribute needed. The Identifier shall provide a clear reference to the Identity System technology used.
Additionally, the process of identifying
an Identity Holder is transparent. It must also be
possible to revoke issued Identity attributes[^18].

[^18]: For more details on Secure Identities, see Plattform Industrie 4.0: Working Group on the Security of Networked Systems. (2016). Technical Overview: Secure Identities. <https://www.plattform-i40.de/PI40/Redaktion/EN/Downloads/Publikation/secure-identities.pdf> as well as Chapter 3.4 in the IAM Framework v1.2: https://community.gaia-x.eu/s/P23ZJNLyjf7n7Zp?path=%2FReleases.

### Layered Identity and Access Management

The Identity and Access Management relies on a two-tiered approach. In practice, this means that Participants use a selected few Identity Systems for mutual identification and trust establishment, SSI being the recommended option for interoperability. After trust is established, underlying existing technologies already in use by Participants (on the "Principal layer") can be federated and reused, for example Open ID Connect or domain specific x509-based communication protocols.

Gaia-X Participants might need to comply with additional requirements on the type and usage of credentials management applications such as mandatory minimum-security requirements, such as Multi-factor authentication. Server-to-Server Communication plays a crucial role in Gaia-X.

This chapter describes the components required to provide an attested secure chain of trust & identities. 
Service implementations and the corresponding service delivery layer may include End-User services, distributed microservice architectures across multiple Participant domains, and/or cross domain data or digital service delivery

#### Architecture principles for this approach

Mutual trust based on mutually verifiable Participant identities between contracting parties, Provider and Consumer, is fundamental to federating trust and identities in the Principal layer.
Heterogeneous ecosystems across multiple identity networks in the Participant layer must be supported as well as heterogeneous environments implementing multiple identity system standards.
The high degree of standardization of Participant layer and Principal layer building blocks of the Gaia-X Trust Framework must ensure that there is no lock-in to any implementation of identity network and Identity System likewise.

![](media/federated_trust_hld.png)

#### Chain of trust and identity

The Gaia-X Participant Identity & Trust framework delivers a secure chain of trust and identities to the service delivery layer.

**Mutual participant verification**

In the Participant layer, the Gaia-X Trust Framework specifies how to resolve and verify the Participant identity of the contracting parties. The Consumer verifies the Provider identity, the Provider verifies the Consumer identity.
Successful mutual Participant verification results in a verified Participant Token representing the trust between Provider and Consumer.

**Identity System federation**

In the Principal layer, the Federation Plugin implements the functionality to federate trust between the Identity Systems of the contracting Participants based on the successful mutual Participant verification described above.  
The federation of trust between the identity systems is based on the identity system standard implemented for the service delivery layer. Required for the federation is a secure mutual exchange of the required federation metadata. This exchange must be secured based on the Verified Participant Token.
Exemplary Identity Systems standards supporting federation are: OIDC/OAuth2 (draft), SAML, SPIFFE/SPIRE. Identity System federation may also include federating the trust between certificate authorities supporting X.509 for Principals.

**Identification and Authentication**

Once successfully federated, the Identity Systems are enabled to identify and authenticate the Principals in the service delivery layer of the contracting parties. Federated Principal identities are mutually trusted based on the federation of the Identity Systems of the contracting Participants.

**Principal Identity Integration Layer**

While the interface to the Gaia-X Federated Trust Component is standardized, the federation mechanism of the Federation Plugin is specific to the implemented Identity System supporting current and future standards like OIDC/OAuth2 (draft), SPIFFE/SPIRE, PKI.
Furthermore, multiple Identity Systems required for complex service offerings, like for example OIDC for user Principals, SPIRE for service Principals, are perfectly supported meaning that multiple Identity Systems on either side can be federated by corresponding plugins based on the very same mutual Participant identity verification if required for the service delivery.

## Data Exchange services

Data Exchange in Gaia-X is enabled by a set of data exchange services that are realized by each participant and can be supported by the Federation.

| Functional needs      | Comment                                                                                              |
|-----------------------|------------------------------------------------------------------------------------------------------|
| Identity & Attributes | A common identity schema and vocabulary for attributes based access.                                 |
| Data protocols        | A common set of protocol and data format, including Enterprise Integration Patterns (EIP)            |
| Policies negotiation and contracting | A common set of Domain Specific Language (DSL) to compute access rights and usage policies           |
| Traceability          | Means to store and trace negotiation results with the capacity to log intermediate realisations, providing an auditable framekwork for transactions |
| Discoverability       | Means to search and find one or more datasets based on queries and filterings, including a common means to declare and specify data ontologies, data vocabularies, and data semantics. |

Each ecosystem is free to implement those functional needs as they see fit.

The Gaia-X Federation Services provides several tools to address those needs:

- a verifiable credential wallet: Organisation Credential Manager (OCM), Personal Credential Manager (PCM)
- policy negociation and logging: Data Contract Transaction (DCT), Data Exchange Logging (DEL)
- a catalogue: Catalogue (CAT)

### Capabilities for Exchange services

The following are essential capabilities for services and products exchange in the
data-infrastructure ecosystems:

| Capability                                        | Description |
|---------------------------------------------------|-------------|
| Expression of Policies in a machine-readable form | To enable transparency and control of service offering usage, it is important to have a common policy specification language to express usage restrictions in a formal and technology-independent manner that is understood and agreed by all Gaia-X Participants. Therefore, they must be formalized and expressed in a common Domain Specific Language, such as ODRL[^23]. |
| Inclusion of Policies in Self-Descriptions         | Informational self-determination and transparency require metadata to describe Resources as well as Providers, Consumers, and Usage Policies as provided by Self-Descriptions and the Federated Catalogues. |
| Interpretation of Usage Policies                  | For a Policy to be agreed upon, it must be understood and agreed by all Participants in a way that enables negotiation and possible technical and organizational enforcement of Policies. |
| Enforcement                                       | Monitoring of services usage is a detective enforcement of service usage with subsequent (compensating) actions. In contrast, preventive enforcement ensures the policy Compliance with technical means (e.g., cancellation or modification of data flows). |

[^23]: W3C. ODRL Information Model 2.2 [W3C Recommendation 15 February 2018]. <https://www.w3.org/TR/odrl-model/>

### Functions of Exchange Services

Information services provide more detailed information about the general
context of the services usage transactions. All information on the services
exchange and services usage transactions must be traceable;  therefore, agreed monitoring
and logging capabilities are required for all services usage transactions. Self-determination also means that Providers can choose to
apply no Usage Policies at all.

The Exchange Services in Gaia-X implement different functions
for different phases of the services exchanges. Therefore, three distinct phases of
service exchanges are defined:

-   before transaction
-   during transaction
-   after transaction

Before the service exchange transaction, the Service Agreement service is
triggered and both parties negotiate a service exchange agreement. This
includes Usage Policies and the required measures to implement those.
During transactions, a Contract Logging service receives logging-messages
that are useful to trace each transaction. This includes data provided,
data received, policy enforced, and policy-violating messages. During
and after the transaction the information stored can be queried by the
transaction partners and a third eligible party, if required.

The figure below
shows the role of the aforementionentioned services to enable controlled data exchange.

![](figures/image9.png)
*Data Services Big Picture*

`Data` are at the core of Data Exchange Services. Data are furnished by `Data Producers` (who are either data owners or data controllers in GDPR sense) to Data Providers who compose these data into a `Data Product` to be used by `Data Consumers`.

A `Data Usage Consent`, including usage terms and conditions associated with these data, is signed by both `Data Producer` and `Data Provider`, and give the `Data Provider` the legal authorization to use these data in accordance with the specified constraints. If a specific `Data License` is attached to the data (for instance when the data is liable to GDPR), then this co-signed `Data Usage Consent` constitutes a legal usage consent and must refer to the explicit license rights from the `Data Licensor` (data subject as per GDPR or data owner). Signed `Data Usage Consents` are notarized by a `Trusted Consent Authority`.

As all Gaia-X entities, `Data Products` are described by a self-description. This self-description is stored in a (searchable) `Federated Data Catalog`. Each `Data Contract` contains a `Data License` defining the usage policy for all data in this `Data Product` – it also contains other information related to billing, technical means, service level, etc. Hence a `Data Contract` constitutes a data usage contract template.

Before using a `Data Product`, the `Data Consumer` negotiate and co-sign a `Data Contrat` with the `Data Provider`. This `Data Contract` may differ from the original `Data Product Self Description`: the `Data License` of the `Data Product Self Description` is sub-licensed, possibly after modification during the negotiations, by enforceable `Terms of Usage contained` in the `Data Contract`. For each licensed data included in the `Data Product`, the `Data Contract` must include an explicit `Data Usage Consent` signed by the corresponding `Data Licensor` (in case of data liable to GDPR, the signed `Data Usage Consent` must contain all information required by the Regulation).

The `Data Contract` is a Ricardian contract: a contract at law that is both human-readable and machine-readable, cryptographically signed and rendered tamper-proof, verifiable in a decentralized fashion, and electronically linked to the subject of the contract, i.e., the data. The parties can (optionally) request this contract to be notarized.

The contract negotiation can lead to both parties agreeing on a `Data Logging Service` which is then used by both sides to log data transmission details.

## Gaia-X Federation Servies for Notarization and Credential storage

Gaia-X AISBL defines a Trust Framework that manifests itself in the form of two services:

- the Gaia-X Registry, detailed in the Operating model chapter
- the Gaia-X Compliance service, as the service implementing the set of rules described in the Gaia-X Trust Framework document.

The Notarization Service will support the issuance of Verifiable Credentials for certain attributes from a Self Description which can be stored in the Personal- or Organizational Credential Manager.

## Portals and APIs

The Portals and API support Participants to interact with 
Federation Services functions via a user interface, which provides
mechanisms to interact with core capabilities using API calls. The goal
is a consistent user experience for all tasks that can be performed with
a specific focus on security and compliance. The Portals provide
information on Resources and Service Offerings and interaction
mechanisms for tasks related to their maintenance, including identity and access management. Each ecosystem can 
deploy its own Portals to support interaction with Federation Services.
