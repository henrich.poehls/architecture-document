# The Gaia-X ecosystem

## Gaia-X as Enabler for ecosystems

The Gaia-X Architecture enables data - infrastructure ecosystems using the elements explained in the [Gaia-X Conceptual Model](conceptual_model.md), the [Gaia-X Operational Model](operating_model.md) and the [Federation Services](conceptual_model.md#federation-services) together with the [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/).

An ecosystem is an organizing principle describing the interaction of different actors and their environment as an integrated whole.
In a technical context, it refers to a set of participants who jointly create an economic community by sharing a common set of rules.

Gaia-X proposes to structure data and infrastructure point of views together as goods and services cannot be handled separately.

The Gaia-X Ecosystem consists of the entirety of all individual ecosystems that use the Architecture and conform to Gaia-X requirements.

![Gaia-X Ecosystem ](figures/Gaia-X_Ecosystem.png)
*Gaia-X Ecosystem Visualization*

Participants in the Gaia-X ecosystems often act as Consumers as well as Providers, the participants may take on both roles described in the conceptual model below. Usually services are composed out of infrastructure, interconnection, software and data services. These service offerings are offered via Federated Catalogues. The common governance includes the Policy Rules, which are statements of objectives, rules, practices or regulations governing the activities of Participants within the Ecosystem and the Trust Framework.

The Gaia-X Framework provides specifications and Open Source Software addressing the key building blocks to create data and infrastructure ecosystems.

![Gaia-X Services](figures/Gaia-X_Services.png)

* providing the basic services to support Data Exchange supporting multipe connector technologies
* supporting the federated propagation of identities, catalogues and integration of the participants systems
* allowing to assess compliance to general Gaia-X rules via the Trust Framework

## Goals of Federation Services

Federation Services aim to enable and facilitate interoperability and
portability of Services and Resources - including Data Resources - within and across Gaia-X-based ecosystems.

They do not interfere with the business models of other members in the
Gaia-X Ecosystem, especially Providers and Consumers. Federation
Services are centrally defined while being federated themselves, so that
they are set up in a federated manner. In this way, they can be used
within individual ecosystems and communities and, through their
federation, enable the sharing of data and services across ecosystems or
communities as well as enable the interoperability and portability of data.

### Avoiding Silos

There may be ecosystems that use the open source Federation Services without being Gaia-X Compliance.
This does not affect the functionality of the Federation Services within specific ecosystems but would hinder their interaction with other ecosystems.
More details in the Federation service chapter.

## Gaia-X Compliance, Data Exchange, and Federations

Gaia-X provides the components to address compliance, federation and interoperable data-exchange: The specifications and the supporting Open Source Code are defined in the Gaia-X Framework

![Gaia-X Framework Context](figures/Gaia-X_Framework_Context.png)

* Data Exchange is addressed as a combination of Gaia-X Data Exchange services and Connectors
* Federation allows Federation of Gaia-X and Ecosystems specific services
* Compliance can be addressed on the level of Conformity to Gaia-X sprecifications, on adherence to the Policy Rules & Label critera as well as towards ruled defined by the ecosystem

## Goals of the Gaia-X Trust Framework

Gaia-X AISBL defines a Trust Framework that manifests itself in the form of two services:

* the Gaia-X Registry, detailed in the Operating model chapter
* the Gaia-X Compliance service, as the service implementing the set of rules described in the upcoming Gaia-X Trust Framework document.

## Interoperability between ecosystems

The Gaia-X Ecosystem is the virtual set of Participants, Service Offerings, Resources fulfilling the requirements of the Gaia-X Trust Framework.  
Gaia-X enables Interoperability between independent autonomous ecosystems.

![Trust Framework](figures/trust framework - main.png)
*Gaia-X planes*

The three planes represent three levels of interoperability and match the planes as described in the NIST Cloud Federation Reference Architecture [chapter 2](https://doi.org/10.6028/NIST.SP.500-332).

### The Trust plane

The Trust plane represents the global digital governance that is shared across ecosystems. The rules of this common governance are captured by the Trust Framework and operationalized by two services:

* the Gaia-X Compliance service, described in the Gaia-X Trust Framework
* the Gaia-X Registry service, describe in the Operational Model chapter of this document

### The Management plane

The Management plane represents an extension of the common digital governance provided by the Federators of the relevant ecosystems.  
It includes potential contract templates specific to a vertical market.  
For example, a finance or a health ecosystem will have additional rules.

Specific ecosystem governance rules are out of scope for Gaia-X.

### The Usage plane

The Usage plane is the one capturing technical interoperability, including the one between Service Offerings.
